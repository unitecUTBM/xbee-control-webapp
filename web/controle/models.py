from django.db import models

# Create your models here.

class Commande(models.Model):
    address = models.CharField(max_length=32)
    x = models.DecimalField(max_digits=4,decimal_places=3)
    y = models.DecimalField(max_digits=4,decimal_places=3)
    def __str__(self):
        return self.address+' ('+str(self.x)+','+str(self.y)+')'
