from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.template import loader

from .models import Commande

# Create your views here.

def index(request):
    '''try:
        cmd=Commande.objects.get(address=request.GET['address'])
    except (KeyError,Commande.DoesNotExist):
        return redirect('http://192.168.43.217:8000/controle/login/')
    context={
        'commande':cmd
    }'''
    if 'address' in request.GET and request.GET['address'] in ['ffff','6354','4876','1276','3972','9753','6782','3945']:
        return render(request,'controle/index.html',{})
    else:
        return redirect('http://192.168.43.217:8000/controle/login/')

def login(request):
    return render(request,'controle/login.html',{})

def data(request):
    try:
        cmd=Commande.objects.get(address=request.GET['address'])
        cmd.x,cmd.y=request.GET['x'],request.GET['y']
        cmd.save()
    except (KeyError,Commande.DoesNotExist):
        return HttpResponse('error')
    return HttpResponse('valid')
