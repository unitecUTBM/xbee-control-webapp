function setup(){
	var canvas=document.getElementById("canvas");
	var context=canvas.getContext("2d");
	var x=0.0,y=0.0,radius=50,debug=false,minUpdateServerTime=50,connected=false,address="";

	var url=new URL(window.location.href);
	address=url.searchParams.get("address");
	debug=url.searchParams.get("debug")==="true";

	var websocket=new WebSocket("ws://"+location.hostname+":8765");
	websocket.onopen=function(event){
		connected=true;
	}

	window.addEventListener("resize",resize,false);
	function resize(){
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
		radius=0.04*Math.sqrt(Math.pow(canvas.width,2)+Math.pow(canvas.height,2));

		draw();
	}
	resize();

	function draw(){
		context.fillStyle="#dddddd";
		context.fillRect(0,0,canvas.width,canvas.height);
		if(debug){
			context.fillStyle="#eeeeee";
			context.fillRect(radius,radius,canvas.width-2*radius,canvas.height-2*radius);
		}
		context.beginPath();
		context.arc(((x+1)/2)*(canvas.width-2*radius)+radius,
				((-y+1)/2)*(canvas.height-2*radius)+radius,
				radius,0,2*Math.PI,false);
		context.fillStyle="#00dd00";
		context.fill();
		context.lineWidth=5;
		context.strokeStyle="#008800";
		context.stroke();

		if(debug){
			context.fillStyle="#333333";
			context.font="30px Arial";
			context.fillText("x:"+x.toFixed(3),5,30);
			context.fillText("y:"+y.toFixed(3),5,60);
		}
	}

	function updateCoordinates(mx,my){
		x=((mx-radius)/(canvas.width-2*radius))*2-1;
		y=-(((my-radius)/(canvas.height-2*radius))*2-1);
		if(x<-1.0) x=-1.0;
		else if(x>1.0) x=1.0;
		if(y<-1.0) y=-1.0;
		else if(y>1.0) y=1.0;
	}

	window.addEventListener("mousemove",mouseMove,false);
	window.addEventListener("mousemove",throttle(updateServer,minUpdateServerTime),false);
	function mouseMove(event){
		updateCoordinates(event.clientX,event.clientY);
		draw();
	}

	window.addEventListener("touchstart",touchEvent,false);
	window.addEventListener("touchstart",throttle(updateServer,minUpdateServerTime),false);
	window.addEventListener("touchmove",touchEvent,false);
	window.addEventListener("touchmove",throttle(updateServer,minUpdateServerTime),false);
	function touchEvent(event){
		var touches=event.touches;
		if(touches.length>0){
			var touch=touches.item(0);
			updateCoordinates(touch.clientX,touch.clientY);
		}
		draw();
	}

	window.addEventListener("touchend",touchReleaseEvent,false);
	window.addEventListener("touchend",throttle(updateServer,minUpdateServerTime),false);
	window.addEventListener("touchcancel",touchReleaseEvent,false);
	window.addEventListener("touchCancel",throttle(updateServer,minUpdateServerTime),false);
	function touchReleaseEvent(event){
		x=0.0,y=0.0;
		draw();
	}

	function updateServer(){
		if(connected){
			websocket.send(address+","+x.toFixed(3)+","+y.toFixed(3));
		}else{
			alert("non connecté");
		}
	}

	function throttle(func,wait){
		var timeout;
		return function(){
			if(!timeout){
				timeout=setTimeout(function(){
					timeout=null;
					func();
				}, wait);
			}
		}
	}
}

window.onload=setup;
