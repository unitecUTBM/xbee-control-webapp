from bitstring import BitArray
import math

def bytes2Str(bytes):
	data=BitArray(hex=bytes).bin
	flip,encoded='',''
	while not (data is ''):
		d=data[:8]
		flip+=d[::-1]
		data=data[8:]
	while not (flip is ''):
		d=flip[:6]
		if len(d)<6:
			d=(d+'000000')[:6]
		d=d[::-1]
		encoded+=charSet[int(d,2)]
		flip=flip[6:]
	return encoded

def getHeader(addr,bytes):
	data=BitArray(hex=bytes).hex
	sum=0
	for byte in zip(data[0::2], data[1::2]):
		sum+=int(''.join(list(byte)),16)
	sum&=0xffff
	return '{:04X}{:04X}{:04X}'.format(addr,math.ceil(len(bytes)/2),sum)

charSet='./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
addr=0x1234
print("data:")
bytes=input()

packet=getHeader(0x1234,bytes)+bytes
encoded=bytes2Str(packet)
print('packet:'+packet)
print('encoded:'+encoded)