import asyncio
import websockets
import os
import socket
from bitstring import BitArray
import math
import serial

#encoding
charSet='./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
def bytes2Str(bytes):
	data=BitArray(hex=bytes).bin
	flip,encoded='',''
	while not (data is ''):
		d=data[:8]
		flip+=d[::-1]
		data=data[8:]
	while not (flip is ''):
		d=flip[:6]
		if len(d)<6:
			d=(d+'000000')[:6]
		d=d[::-1]
		encoded+=charSet[int(d,2)]
		flip=flip[6:]
	return encoded

def getHeader(addr,bytes):
	data=BitArray(hex=bytes).hex
	sum=0
	for byte in zip(data[0::2], data[1::2]):
		sum+=int(''.join(list(byte)),16)
	sum&=0xffff
	return '{:04X}{:04X}{:04X}'.format(addr,math.ceil(len(bytes)/2),sum)

#websocket
whitelist=['ffff','6354','4876','1276','3972','9753','6782','3945']
dt=1/10
robots=dict(zip(whitelist,[[0,0]]*len(whitelist)))

'''addr=0x1234
print("data:")
bytes=input()

packet=getHeader(0x1234,bytes)+bytes
encoded=bytes2Str(packet)
print('packet:'+packet)
print('encoded:'+encoded)'''

async def getData(websocket,path):
	try:
		while True:
			data=await websocket.recv()
			[address,x,y]=data.split(',')
			if address in robots:
				robots[address]=[float(x),float(y)]
				#print("{}: {}".format(address,robots[address]))
	except websockets.exceptions.ConnectionClosed:
		return

async def sendData(*args):
	while True:
		for address,pos in robots.items():
			hexData='{:02X}{:02X}'.format(int(round(((pos[0]+1)/2)*255,0)),int(round(((pos[1]+1)/2)*255,0)))
			packet=getHeader(int(address,16),hexData)+hexData
			enc=bytes2Str(packet)
			print(address,pos,enc)
			xbee.write(str.encode(enc+'\n'))
		await asyncio.sleep(dt)

with serial.Serial(port='/dev/ttyUSB0',baudrate=57600) as xbee:

	ip=(([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]
	print('start with ip {}'.format(ip))
	start_server = websockets.serve(getData,'192.168.1.143',8765)

	loop=asyncio.get_event_loop()
	loop.run_until_complete(start_server)
	loop.run_until_complete(asyncio.gather(sendData()))
	loop.run_forever()
