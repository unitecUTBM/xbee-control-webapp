import os
from threading import Thread

def startServer():
	os.system('python3 manage.py runserver 0:8000')

def startWebSocket():
	os.system('sudo python3 websocketServer.py')

threadWeb=Thread(target=startServer)
threadWeb.start()
threadSocket=Thread(target=startWebSocket)
threadSocket.start()

